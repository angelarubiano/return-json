import express from 'express';
import get_sitio from './get_sitio.js';

const app = express();

let puerto = 3005;

//Definir ruta

app.get("/get_sitio", (req, res) => {
    res.send(get_sitio(req));
});

// Iniciar un servidor

app.listen(puerto, () => { console.log("Esta funcionando el servidor") });
